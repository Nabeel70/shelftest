import { useState, useCallback } from "react";
import CollaborationsPopup from "../components/collaborations-popup";
import PortalPopup from "../components/portal-popup";
import { useRouter } from "next/router";
import Link from "next/link";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./collaboration.module.css";

const Collaboration = () => {
  const [isCollaborationsPopupOpen, setCollaborationsPopupOpen] =
    useState(false);
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);
  const [isCollaborationsPopup1Open, setCollaborationsPopup1Open] =
    useState(false);

  const openCollaborationsPopup = useCallback(() => {
    setCollaborationsPopupOpen(true);
  }, []);

  const closeCollaborationsPopup = useCallback(() => {
    setCollaborationsPopupOpen(false);
  }, []);

  const onIconButtonClick = useCallback(() => {
    router.push("/collaborations");
  }, [router]);

  const onMapViewButtonClick = useCallback(() => {
    router.push("/collaborations");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const openCollaborationsPopup1 = useCallback(() => {
    setCollaborationsPopup1Open(true);
  }, []);

  const closeCollaborationsPopup1 = useCallback(() => {
    setCollaborationsPopup1Open(false);
  }, []);

  return (
    <>
      <main
        className={styles.collaborationMain}
        id="main"
        onClick={openCollaborationsPopup1}
      >
        <iframe
          className={styles.rectangleIframe}
          src={`https://maps.google.com/maps?q=charing%20cross&t=&z=13&ie=UTF8&iwloc=&output=embed`}
          onClick={openCollaborationsPopup}
        />
        <img
          className={styles.locationOnIcon}
          alt=""
          src="../location-on2.svg"
        />
        <button className={styles.iconButton} onClick={onIconButtonClick}>
          <img
            className={styles.arrowForwardIosIcon}
            alt=""
            src="../arrow-forward-ios2.svg"
          />
        </button>
        <img className={styles.zoomOutIcon} alt="" src="../zoom-out2.svg" />
        <img className={styles.zoomInIcon} alt="" src="../zoom-in2.svg" />
        <button className={styles.mapViewButton} onClick={onMapViewButtonClick}>
          <p className={styles.labelTextP}>Map View: Collaborations</p>
        </button>
        <div className={styles.headerMainsiteDiv}>
          <Link href="/">
            <a className={styles.shareAshelf}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan}>share</span>
                <span className={styles.aSpan}>A</span>
                <span className={styles.shelfSpan}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <div className={styles.groupDiv}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv}></div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </div>
          </button>
          <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
          <Link href="/signup">
            <a className={styles.signUpA}>Sign Up:</a>
          </Link>
        </div>
        <div className={styles.formFooterDiv}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv} />
        </div>
      </main>
      {isCollaborationsPopupOpen && (
        <PortalPopup
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Centered"
          onOutsideClick={closeCollaborationsPopup}
        >
          <CollaborationsPopup onClose={closeCollaborationsPopup} />
        </PortalPopup>
      )}
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
      {isCollaborationsPopup1Open && (
        <PortalPopup
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Centered"
          onOutsideClick={closeCollaborationsPopup1}
        >
          <CollaborationsPopup onClose={closeCollaborationsPopup1} />
        </PortalPopup>
      )}
    </>
  );
};

export default Collaboration;
