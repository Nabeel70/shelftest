import { useState, useCallback } from "react";
import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Select } from "antd";
import { Form } from "react-bootstrap";
import HostPopup from "../components/host-popup";
import PortalPopup from "../components/portal-popup";
import { useRouter } from "next/router";
import Link from "next/link";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./hosts.module.css";

const Hosts = () => {
  const [isHostPopupOpen, setHostPopupOpen] = useState(false);
  const [isHostPopup1Open, setHostPopup1Open] = useState(false);
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const openHostPopup = useCallback(() => {
    setHostPopupOpen(true);
  }, []);

  const closeHostPopup = useCallback(() => {
    setHostPopupOpen(false);
  }, []);

  const openHostPopup1 = useCallback(() => {
    setHostPopup1Open(true);
  }, []);

  const closeHostPopup1 = useCallback(() => {
    setHostPopup1Open(false);
  }, []);

  const onIconButtonClick = useCallback(() => {
    router.push("/collaboration");
  }, [router]);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <div className={styles.hostsDiv} id="main">
        <div className={styles.rectangleDiv} onClick={openHostPopup} />
        <div className={styles.floatingLabelDiv}>
          <b className={styles.label}>Filters</b>
        </div>
        <h6 className={styles.collaborationDatesH6}>Collaboration Dates:</h6>
        <strong className={styles.augustStrong}>12-19 August</strong>
        <p className={styles.trailingDataP}>Cafe</p>
        <Form.Check
          className={styles.trailingIconFormCheck}
          name="cafe"
          id="cafe"
          isInvalid
        />
        <p className={styles.trailingDataP1}>Restaurant</p>
        <Form.Check
          className={styles.trailingIconFormCheck1}
          name="restaurant"
          id="restaurant"
          isInvalid
        />
        <p className={styles.trailingDataP2}>Venue</p>
        <Form.Check
          className={styles.trailingIconFormCheck2}
          name="venue"
          id="venue"
          isInvalid
        />
        <p className={styles.trailingDataP3}>Community</p>
        <p className={styles.trailingDataP4}>Studios</p>
        <Form.Check
          className={styles.trailingIconFormCheck3}
          name="studios"
          id="studios"
          isInvalid
        />
        <Form.Check
          className={styles.trailingIconFormCheck4}
          name="community"
          id="community"
          isInvalid
        />
        <p className={styles.trailingDataP5}>Co-Work</p>
        <Form.Check
          className={styles.trailingIconFormCheck5}
          name="co-work"
          id="co-work"
          isInvalid
        />
        <h6 className={styles.hostCategoriesH6}>Host Categories:</h6>
        <div className={styles.lineDiv} />
        <div className={styles.mapSectionDiv}>
          <iframe
            className={styles.rectangleIframe}
            src={`https://maps.google.com/maps?q=charring%20cross&t=&z=15&ie=UTF8&iwloc=&output=embed`}
            onClick={openHostPopup1}
          />
          <button className={styles.iconButton} onClick={onIconButtonClick}>
            <img
              className={styles.arrowForwardIosIcon}
              alt=""
              src="../arrow-forward-ios3.svg"
            />
          </button>
          <img className={styles.zoomOutIcon} alt="" src="../zoom-out3.svg" />
          <img className={styles.zoomInIcon} alt="" src="../zoom-in3.svg" />
          <img
            className={styles.locationOnIcon}
            alt=""
            src="../location-on3.svg"
          />
        </div>
        <div className={styles.footerHostsDiv}>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.shareAshelfLtdDiv}>
            © 2022 shareAshelf Ltd.
          </div>
        </div>
        <div className={styles.frameDiv}>
          <Link href="/">
            <a className={styles.shareAshelf} onClick={onShareAshelfLinkClick}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan}>share</span>
                <span className={styles.aSpan}>A</span>
                <span className={styles.shelfSpan}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <div className={styles.groupDiv}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv}></div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </div>
          </button>
          <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
          <Link href="/signup">
            <a className={styles.signUpA} onClick={onSignUpLinkClick}>
              Sign Up:
            </a>
          </Link>
        </div>
        <div className={styles.mobileMenuDiv}>
          <div className={styles.fieldDiv}>
            <div className={styles.frameDiv1}>
              <div className={styles.contentDiv}>
                <div className={styles.floatingLabelDiv1}>
                  <b className={styles.label}>Map View</b>
                </div>
                <div className={styles.inputDiv}>
                  <b className={styles.inputB} />
                </div>
              </div>
            </div>
            <Select
              className={styles.inputSelect}
              style={{ width: "219px" }}
              size="middle"
              placeholder="Hosts"
              bordered={false}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              virtual={true}
              showArrow={true}
            >
              <Select.Option value="Collaborations">
                Collaborations
              </Select.Option>
              <Select.Option value="Hosts">Hosts</Select.Option>
              <Select.Option value="Vendors">Vendors</Select.Option>
            </Select>
          </div>
        </div>
      </div>
      {isHostPopupOpen && (
        <PortalPopup
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Centered"
          onOutsideClick={closeHostPopup}
        >
          <HostPopup onClose={closeHostPopup} />
        </PortalPopup>
      )}
      {isHostPopup1Open && (
        <PortalPopup
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Centered"
          onOutsideClick={closeHostPopup1}
        >
          <HostPopup onClose={closeHostPopup1} />
        </PortalPopup>
      )}
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default Hosts;
