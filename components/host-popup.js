import { useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styles from "./host-popup.module.css";

const HostPopup = ({ onClose }) => {
  const router = useRouter();

  const onButtonClick = useCallback(() => {
    router.push("/hosts-profile");
  }, [router]);

  return (
    <div className={styles.hostPopupDiv}>
      <div className={styles.headerDiv}>
        <div className={styles.contentDiv}>
          <div className={styles.textDiv}>
            <h6 className={styles.headerH6}>Checkmate</h6>
            <p className={styles.subheadP}>Gaming Cafe</p>
          </div>
        </div>
        <Link href="/hosts">
          <a className={styles.closeA} onClick={onClose}>
            <img className={styles.closeIcon} alt="" src="../close.svg" />
          </a>
        </Link>
      </div>
      <img className={styles.mediaIcon} alt="" src="../media@2x.png" />
      <div className={styles.supportingTextDiv}>
        <p className={styles.supportingText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </p>
      </div>
      <button className={styles.button} autoFocus onClick={onButtonClick}>
        <div className={styles.labelTextDiv}>See Profile</div>
      </button>
      <div className={styles.buttonDiv}>
        <div className={styles.labelTextDiv1}>Enabled</div>
      </div>
    </div>
  );
};

export default HostPopup;
