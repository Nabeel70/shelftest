import { useCallback, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styles from "./menu-drawer.module.css";

const MenuDrawer = ({ onClose }) => {
  const router = useRouter();

  const onSignupLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  const onMainLinkClick = useCallback(() => {
    router.push("/home");
  }, [router]);

  const onHostsLinkClick = useCallback(() => {
    router.push("/hosts");
  }, [router]);

  const onVendorsLinkClick = useCallback(() => {
    router.push("/vendors");
  }, [router]);

  const onFAQSLinkClick = useCallback(() => {
    router.push("/faqs");
  }, [router]);

  const onHomeLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  useEffect(() => {
    const scrollAnimElements = document.querySelectorAll(
      "[data-animate-on-scroll]"
    );
    const observer = new IntersectionObserver(
      (entries) => {
        for (const entry of entries) {
          if (entry.isIntersecting || entry.intersectionRatio > 0) {
            const targetElement = entry.target;
            targetElement.classList.add(styles.animate);
            observer.unobserve(targetElement);
          }
        }
      },
      {
        threshold: 0.15,
      }
    );

    for (let i = 0; i < scrollAnimElements.length; i++) {
      observer.observe(scrollAnimElements[i]);
    }

    return () => {
      for (let i = 0; i < scrollAnimElements.length; i++) {
        observer.unobserve(scrollAnimElements[i]);
      }
    };
  }, []);

  return (
    <div className={styles.menuDrawerDiv} data-animate-on-scroll>
      <div className={styles.frameDiv}>
        <div className={styles.frameDiv}>
          <div className={styles.rectangleDiv} />
          <Link href="/signup">
            <a className={styles.signupA} onClick={onSignupLinkClick}>
              Signup
            </a>
          </Link>
          <Link href="/home">
            <a className={styles.main} onClick={onMainLinkClick}>
              Main
            </a>
          </Link>
          <Link href="/hosts">
            <a className={styles.hostsA} onClick={onHostsLinkClick}>
              Hosts
            </a>
          </Link>
          <a className={styles.vendorsA} onClick={onVendorsLinkClick}>
            Vendors
          </a>
          <Link href="/faqs">
            <a className={styles.fAQS} onClick={onFAQSLinkClick}>
              FAQ’S
            </a>
          </Link>
        </div>
      </div>
      <Link href="/">
        <a className={styles.homeA} onClick={onHomeLinkClick}>
          Home
        </a>
      </Link>
    </div>
  );
};

export default MenuDrawer;
